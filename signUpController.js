const objController = require('./usersController')
const jwt = require("jsonwebtoken")
const bcrypt = require("bcryptjs")

class SignUp {
    constructor() {
    }

}

const signUpUser = (body, res) => {
    const { id, pass } = body;
    if (id && pass) {
        user = objController.obtainUserById(id);
        if(user !== undefined) {
            //const salt = await bcrypt.genSalt(10);
            //user.pass = bcrypt.hash(pass, salt);
            user.pass = pass;
            objController.patchUser(body, res)
            const token = jwt.sign({id}, 'SECRET',{
                    expiresIn: 86400 //24hs
            })
            console.log('token: ', token);
            //res.status(200).json({token});
        }else{
            res.status(404).send('User Not Found')
        }
    } else {
        res.status(400).send('Bad Request')
    }
};

const signUpController = (req, res) => {
    switch (req.method){
        case "POST":
            signUpUser(req.body, res);
            break;
        default:
            console.log("peticion no soportada");
    }
};

module.exports = signUpController;
