FROM tarampampam/node:13-alpine

WORKDIR /home/rest-example-node/

COPY . .

RUN npm install

EXPOSE 4000

CMD [ "npm", "start" ]
