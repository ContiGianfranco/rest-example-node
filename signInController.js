const objController = require('./usersController');
const jwt = require("jsonwebtoken")

class SignIn {
    constructor() {
    }

}

const signInUser = (body, res) => {
    const { id, pass } = body;
    if (id && pass) {
        user = objController.obtainUserById(id);
        if(user !== undefined) {
            if(pass !== user.pass){
                return res.status(401).json({token: null, message: 'Invalid Password'})
            }else{
                const token = jwt.sign({id}, 'SECRET',{
                    expiresIn: 86400 //24hs
                })
                return (res.status(200).json({token}));
            }
        }else {
            res.status(404).send('User Not Found')
        }
    } else {
        res.status(400).send('Bad Request')
    }
}

const signInController = (req, res) => {
    switch (req.method){
        case "POST":
            signInUser(req.body, res);
            break;
        default:
            console.log("peticion no soportada");
    }
};

module.exports = signInController;
