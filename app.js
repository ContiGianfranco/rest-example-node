const express = require('express');
const cors = require('cors');
const PORT = 4000;

const greetingController = require('./greetingController');
const objController = require('./usersController');
const signUpController = require("./signUpController");
const signInController = require("./signInController");
const tokenVerifier = require("./verifyToken");

const app = express();
app.use(express.json());
app.use(cors());

app.get('/greeting', greetingController);
app.get('/users', objController.usersController);
app.post('/users', objController.usersController);
app.delete('/users', tokenVerifier, objController.usersController);
app.patch('/users', objController.usersController);
app.post('/signup', signUpController);
app.post('/signin', signInController);

app.listen(PORT, () => {
  console.log(`rest-example-node listening on port ${PORT}!`);
});
