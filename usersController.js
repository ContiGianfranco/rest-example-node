let userIduserId = 0;
var users = {};

class User {
    constructor(body) {
        if (Object.keys(body).length !== 4) {
            throw new Error('Invalid number of arguments');
        }
        const { name, lastName, avatarPhoto, birthday } = body;
        if(name && lastName && avatarPhoto && birthday){
            this.id = userIduserId;
            this.name = body.name;
            this.lastName = body.lastName;
            this.avatarPhoto = body.avatarPhoto;
            this.birthday = body.birthday;
            this.pass = '';
        } else {
            throw new Error('Invalid arguments');
        }
    }

    get(){
        return {
            id: this.id,
            name: this.name,
            lastName: this.lastName,
            avatarPhoto: this.avatarPhoto,
            birthday: this.birthday,
            pass: this.pass,
        };
    }

    patchField(field, newValue) {
        this[field] = newValue
    }
}

const createUser = (body, res) => {
    userIduserId++;
    try {
        users[userIduserId] = new User(body);
    } catch (e){
        userIduserId--;
        console.error(e.name + ': ' + e.message);
        res.status(400).send('Bad Request. ' + e.name + ': ' + e.message);
    }
    res.status(200).send(users[userIduserId].get());
};

const getUser = (body, res) => {
    try {
        const { id } = body;
        if (id) {
            res.status(200).send(users[id].get());
        } else {
            console.error(e.name + ': ' + e.message);
            res.status(404).send('Invalid arguments');
        }
    } catch (e){
        console.error(e.name + ': ' + e.message);
        res.status(404).send('User Not Found');
    }
}

const deleteUser  = (body, res) => {
    const { id } = body;
    if (id) {
        if (users.hasOwnProperty(id)) {
            delete users[id];
            res.status(200).send('Success');
        } else {
            res.status(404).send('User Not Found')
        }
    } else {
        console.error(e.name + ': ' + e.message);
        res.status(404).send('Invalid arguments');
    }
}

const patchUser = (body, res) => {
    for (let key in body) {
        if (users[body.id].hasOwnProperty(key)){
            users[body.id].patchField(key, body[key])
        }
    }
    res.status(200).send(users[body.id].get());
}

const obtainUserById = (id) => {
    if (users.hasOwnProperty(id)) {
        return (users[id].get());
    }else{
        return null;
    }
}

const usersController = (req, res) => {
    switch (req.method){
        case "POST":
            createUser(req.body, res);
            break;
        case "GET":
            getUser(req.body, res);
            break;
        case "DELETE":
            deleteUser(req.body, res);
            break;
        case "PATCH":
            patchUser(req.body, res);
            break;
        default:
            console.log("peticion no soportada");
    }
};

module.exports.usersController = usersController;

module.exports.obtainUserById = obtainUserById;

module.exports.patchUser = patchUser;
