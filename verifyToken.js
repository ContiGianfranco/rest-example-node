const objController = require('./usersController');
const jwt = require("jsonwebtoken")


const verifyToken = (req, res, next) => {
    try {
        const token = req.headers["x-access-token"];

        if (token === undefined) {
            return (res.status(403).send('No token provided.'));
        }

        const decoded = jwt.verify(token, 'SECRET');
        const user = objController.obtainUserById(decoded.id);
        console.log(decoded.id);
        if (user === undefined) {
            return (res.status(404).send('No user founded.'));
        }
        next();
    } catch (e){
        return (res.status(401).send('Unauthorized.'));
    }
};

module.exports = verifyToken;
